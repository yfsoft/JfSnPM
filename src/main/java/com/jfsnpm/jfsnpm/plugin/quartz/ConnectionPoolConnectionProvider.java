package com.jfsnpm.jfsnpm.plugin.quartz;

import java.sql.Connection;
import java.sql.SQLException;
import org.quartz.utils.ConnectionProvider;
import com.jfsnpm.jfsnpm.core.AppConfig;


public class ConnectionPoolConnectionProvider implements ConnectionProvider {

	@Override
	public Connection getConnection() throws SQLException {
		return AppConfig.c3p0Plugin.getDataSource().getConnection();
	}
	@Override
	public void initialize() throws SQLException {
		
	}
	@Override
	public void shutdown() throws SQLException {
		
	}

}
